package com.sevikolar.sevitrade.domain.enums;

public enum OrderAction {
    BUY,
    SELL
}
