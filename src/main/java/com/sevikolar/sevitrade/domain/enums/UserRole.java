package com.sevikolar.sevitrade.domain.enums;

public enum UserRole {
    ROLE_ADMIN,
    ROLE_USER
}
