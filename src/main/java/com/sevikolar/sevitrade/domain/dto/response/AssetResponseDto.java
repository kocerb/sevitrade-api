package com.sevikolar.sevitrade.domain.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AssetResponseDto {

    UUID id;
    UUID symbolId;
    String symbolCode;
    String symbolTitle;
    Double quantity;
}
