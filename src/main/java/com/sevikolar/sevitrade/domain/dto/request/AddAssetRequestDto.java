package com.sevikolar.sevitrade.domain.dto.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AddAssetRequestDto {

    private Double quantity;
    private UUID symbolId;
}
