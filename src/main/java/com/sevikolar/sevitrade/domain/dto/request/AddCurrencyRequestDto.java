package com.sevikolar.sevitrade.domain.dto.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AddCurrencyRequestDto {

    private String code;
    private String title;
}
