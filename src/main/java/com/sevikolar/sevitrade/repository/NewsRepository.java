package com.sevikolar.sevitrade.repository;

import com.sevikolar.sevitrade.domain.entity.News;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface NewsRepository extends JpaRepository<News, UUID> {
}
