package com.sevikolar.sevitrade.repository;

import com.sevikolar.sevitrade.domain.entity.Symbol;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface SymbolRepository extends JpaRepository<Symbol, UUID> {
}
