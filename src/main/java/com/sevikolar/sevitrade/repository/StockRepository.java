package com.sevikolar.sevitrade.repository;

import com.sevikolar.sevitrade.domain.entity.Stock;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface StockRepository extends JpaRepository<Stock, UUID> {
}