package com.sevikolar.sevitrade.repository;

import com.sevikolar.sevitrade.domain.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface UserRepository extends JpaRepository<User, UUID> {

    Optional<User> findByEmail(String email);
    User getByEmail(String email);
}
