package com.sevikolar.sevitrade.repository;

import com.sevikolar.sevitrade.domain.entity.Currency;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface CurrencyRepository extends JpaRepository<Currency, UUID> {
}
