package com.sevikolar.sevitrade.repository;

import com.sevikolar.sevitrade.domain.entity.Asset;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

public interface AssetRepository  extends JpaRepository<Asset, UUID> {

    List<Asset> findAllByUser_Email(String email);
}
