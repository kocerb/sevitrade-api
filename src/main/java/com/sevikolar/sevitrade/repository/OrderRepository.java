package com.sevikolar.sevitrade.repository;

import com.sevikolar.sevitrade.domain.entity.Order;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface OrderRepository extends JpaRepository<Order, UUID> {
}
