package com.sevikolar.sevitrade.repository;

import com.sevikolar.sevitrade.domain.entity.Company;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface CompanyRepository extends JpaRepository<Company, UUID> {
}
