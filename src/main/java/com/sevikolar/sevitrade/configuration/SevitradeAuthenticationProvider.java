package com.sevikolar.sevitrade.configuration;

import com.sevikolar.sevitrade.domain.entity.User;
import com.sevikolar.sevitrade.repository.UserRepository;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Component
public class SevitradeAuthenticationProvider implements AuthenticationProvider {

    private final UserRepository userRepository;

    public SevitradeAuthenticationProvider(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String email = authentication.getName();
        String password = authentication.getCredentials().toString();

        Optional<User> userOptional = this.userRepository.findByEmail(email);

        if (userOptional.isPresent()) {
            User user = userOptional.get();

            if (user.getPassword().equals(password)) {
                List<GrantedAuthority> roleList = Collections.singletonList(new SimpleGrantedAuthority(user.getRole().toString()));
                return new UsernamePasswordAuthenticationToken(email, password, roleList);
            } else {
                throw new BadCredentialsException("PAROLA YANLIŞ");
            }
        } else {
            throw new UsernameNotFoundException("BÖYLE BİR KULLANICI YOK!");
        }
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }
}
