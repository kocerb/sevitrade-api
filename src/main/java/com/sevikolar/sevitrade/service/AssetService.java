package com.sevikolar.sevitrade.service;

import com.sevikolar.sevitrade.domain.dto.request.AddAssetRequestDto;
import com.sevikolar.sevitrade.domain.dto.response.AssetResponseDto;
import com.sevikolar.sevitrade.domain.entity.Asset;
import com.sevikolar.sevitrade.domain.entity.Symbol;
import com.sevikolar.sevitrade.domain.entity.User;
import com.sevikolar.sevitrade.repository.AssetRepository;
import com.sevikolar.sevitrade.repository.CurrencyRepository;
import com.sevikolar.sevitrade.repository.SymbolRepository;
import com.sevikolar.sevitrade.repository.UserRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class AssetService {

    private final AssetRepository assetRepository;
    private final CurrencyRepository currencyRepository;
    private final SymbolRepository symbolRepository;
    private final UserRepository userRepository;

    public AssetService(AssetRepository assetRepository, CurrencyRepository currencyRepository, SymbolRepository symbolRepository, UserRepository userRepository) {
        this.assetRepository = assetRepository;
        this.currencyRepository = currencyRepository;
        this.symbolRepository = symbolRepository;
        this.userRepository = userRepository;
    }

    public AssetResponseDto addAssetToUser(AddAssetRequestDto addAssetRequestDto, String email) {
        Asset asset = new Asset();
        asset.setQuantity(addAssetRequestDto.getQuantity());

        Optional<Symbol> symbolOptional = symbolRepository.findById(addAssetRequestDto.getSymbolId());
        if (symbolOptional.isPresent()) {
            asset.setSymbol(symbolOptional.get());
        }

        User user = userRepository.getByEmail(email);
        asset.setUser(user);

        Asset savedAsset = assetRepository.save(asset);

        AssetResponseDto assetResponseDto = new AssetResponseDto();
        assetResponseDto.setId(savedAsset.getId());
        assetResponseDto.setQuantity(savedAsset.getQuantity());
        assetResponseDto.setSymbolId(savedAsset.getSymbol().getId());
        assetResponseDto.setSymbolCode(savedAsset.getSymbol().getCode());
        assetResponseDto.setSymbolTitle(savedAsset.getSymbol().getTitle());

        return assetResponseDto;
    }

    public List<AssetResponseDto> getAllAssetsOfUser(String email) {
        List<Asset> assetList = assetRepository.findAllByUser_Email(email);
        List<AssetResponseDto> assetResponseDtoList = new ArrayList<>();

        assetList.forEach(asset -> {
            AssetResponseDto assetResponseDto = new AssetResponseDto();
            assetResponseDto.setId(asset.getId());
            assetResponseDto.setQuantity(asset.getQuantity());
            assetResponseDto.setSymbolId(asset.getSymbol().getId());
            assetResponseDto.setSymbolCode(asset.getSymbol().getCode());
            assetResponseDto.setSymbolTitle(asset.getSymbol().getTitle());

            assetResponseDtoList.add(assetResponseDto);
        });

        return assetResponseDtoList;
    }

}
