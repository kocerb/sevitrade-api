package com.sevikolar.sevitrade.controller;

import com.sevikolar.sevitrade.domain.dto.request.AddCurrencyRequestDto;
import com.sevikolar.sevitrade.domain.entity.Currency;
import com.sevikolar.sevitrade.repository.CurrencyRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping("currencies")
public class CurrencyController {

    private final CurrencyRepository currencyRepository;

    CurrencyController(CurrencyRepository currencyRepository) {
        this.currencyRepository = currencyRepository;
    }

    @PostMapping
    @PreAuthorize("hasRole('ADMIN')")
    Currency addCurrency(@RequestBody AddCurrencyRequestDto addCurrencyRequestDto) {
        Currency currency = new Currency();
        currency.setCode(addCurrencyRequestDto.getCode());
        currency.setTitle(addCurrencyRequestDto.getTitle());

        return this.currencyRepository.save(currency);
    }

    @GetMapping
    List<Currency> getAllCurrencies() {
        return this.currencyRepository.findAll();
    }

    @GetMapping
    @RequestMapping(value = "/{id}")
    ResponseEntity<Currency> getCurrency(@PathVariable UUID id) {
        Optional<Currency> currencyOptional = this.currencyRepository.findById(id);

        if (currencyOptional.isPresent()) {
            return ResponseEntity.ok(currencyOptional.get());
        } else {
            return ResponseEntity.notFound().build();
        }
    }
}
