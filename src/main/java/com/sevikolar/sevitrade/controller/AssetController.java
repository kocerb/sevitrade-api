package com.sevikolar.sevitrade.controller;

import com.sevikolar.sevitrade.domain.dto.request.AddAssetRequestDto;
import com.sevikolar.sevitrade.domain.dto.response.AssetResponseDto;
import com.sevikolar.sevitrade.domain.entity.Asset;
import com.sevikolar.sevitrade.repository.AssetRepository;
import com.sevikolar.sevitrade.repository.CurrencyRepository;
import com.sevikolar.sevitrade.repository.SymbolRepository;
import com.sevikolar.sevitrade.repository.UserRepository;
import com.sevikolar.sevitrade.service.AssetService;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("assets")
public class AssetController {

    private final AssetService assetService;

    AssetController(AssetService assetService) {
        this.assetService = assetService;
    }

    @GetMapping
    List<AssetResponseDto> getAllAsset(Authentication authentication) {
        return assetService.getAllAssetsOfUser(authentication.getName());
    }

    @PostMapping
    AssetResponseDto addAsset(@RequestBody AddAssetRequestDto addAssetRequestDto, Authentication authentication) {
        return assetService.addAssetToUser(addAssetRequestDto, authentication.getName());
    }
}

