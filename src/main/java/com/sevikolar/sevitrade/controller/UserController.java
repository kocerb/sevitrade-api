package com.sevikolar.sevitrade.controller;

import com.sevikolar.sevitrade.domain.dto.request.AddUserRequestDto;
import com.sevikolar.sevitrade.domain.entity.User;
import com.sevikolar.sevitrade.repository.UserRepository;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("users")
public class UserController {

    private final UserRepository userRepository;

    public UserController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @PostMapping
    User addUser(@RequestBody AddUserRequestDto addUserRequestDto) {
        User user = new User();
        user.setEmail(addUserRequestDto.getEmail());

        User savedUser = this.userRepository.save(user);
        return savedUser;
    }
}
