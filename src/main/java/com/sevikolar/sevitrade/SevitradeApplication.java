package com.sevikolar.sevitrade;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SevitradeApplication {

	public static void main(String[] args) {
		SpringApplication.run(SevitradeApplication.class, args);
	}

}
